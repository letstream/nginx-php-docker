# Docker Image + Kubernetes Config for quick WordPress Deployment (including MySQL Server)

## Quick Deploy

- Navigate to k8s directory
- Paste your SSL Certificate and Key for your domain in k8s/certs respective file.
- Replace `<namespace>` with your own kubernetes namespace in all files.
- Replace `nsf-storageclass-retain` with your own StorageClass
- Replace `example.com` and `www.example.com` with your own domain name in Ingress Config in deploy.yaml
- Replace `<MYSQL_ROOT_PASS>` with your desired ROOT MySQL Password in mysql-values.yaml
- Replace `<DATABASE_NAME>`, `<DATABASE_USER>`, and `<DATABASE_PASS>` with your desired values in mysql-values.yaml

Now execute file `k8s/deploy.sh` to create namespace, deploy MySQL Server, and create required resources for WordPress
All done!


Note: If you don't want to create a fresh installation, you can remove the `wp-downloader` Job Specification from deploy.yaml, and use `kubectl cp` to copy existing wordpress files to `/usr/share/nginx/html` directory of the container.

---
Maintained by [Letstream Ventures Pvt Ltd](https://www.theletstream.com/)
